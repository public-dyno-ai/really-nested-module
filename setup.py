from setuptools import setup

setup(name='reallyNestedModule',
      version='0.1',
      description='Example of how to really nest a module',
      url='http://gitlab.com/dynoai/really-nested-module',
      author='Danny Hui',
      author_email='danny@dyno.ai',
      license='MIT',
      packages=['nest','nest.leveltwo','nest.leveltwo.levelthree'],
      zip_safe=False)
